pragma solidity ^0.4.11;

contract Voting {
  mapping(bytes32 => uint8) public votesReceived;
  bytes32[] public candidateList; 

  function Voting(bytes32[] candidateNames) public {
    candidateList = candidateNames;
  }

  function totalVotesFor(bytes32 candidate) public returns (uint8) {
    return  votesReceived[candidate];
  }

  function validCandidate(bytes32 candidate) private returns (bool) {
    for (uint index = 0; index < candidateList.length; index++) {
      if (candidateList[index] === candidate) {
        return true;
      }
    }

    return false;
  }

  function voteForCandidate(bytes32 candidate) public {
    if (validCandidate(candidate) == false) throw;
    votesReceived[candidate]++;
  }
}