const web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:5500/voting.sol'));
const abi = [
  {
    'constant': false,
    'inputs': [
      {
        'name': 'candidate',
        'type': 'bytes32'
      }
    ],
    'name': 'totalVotesFor',
    'outputs': [
      {
        'name': '',
        'type': 'uint8'
      }
    ],
    'payable': false,
    'type': 'function'
  },
  {
    'constant': false,
    'inputs': [
      {
        'name': 'candidate',
        'type': 'bytes32'
      }
    ],
    'name': 'validCandidate',
    'outputs': [
      {
        'name': '',
        'type': 'bool'
      }
    ],
    'payable': false,
    'type': 'function'
  },
  {
    'constant': true,
    'inputs': [
      {
        'name': '',
        'type': 'bytes32'
      }
    ],
    'name': 'votesReceived',
    'outputs': [
      {
        'name': '',
        'type': 'uint8'
      }
    ],
    'payable': false,
    'type': 'function'
  },
  {
    'constant': true,
    'inputs': [
      {
        'name': 'x',
        'type': 'bytes32'
      }
    ],
    'name': 'bytes32ToString',
    'outputs': [
      {
        'name': '',
        'type': 'string'
      }
    ],
    'payable': false,
    'type': 'function'
  },
  {
    'constant': true,
    'inputs': [
      {
        'name': '',
        'type': 'uint256'
      }
    ],
    'name': 'candidateList',
    'outputs': [
      {
        'name': '',
        'type': 'bytes32'
      }
    ],
    'payable': false,
    'type': 'function'
  },
  {
    'constant': false,
    'inputs': [
      {
        'name': 'candidate',
        'type': 'bytes32'
      }
    ],
    'name': 'voteForCandidate',
    'outputs': [],
    'payable': false,
    'type': 'function'
  },
  {
    'constant': true,
    'inputs': [],
    'name': 'contractOwner',
    'outputs': [
      {
        'name': '',
        'type': 'address'
      }
    ],
    'payable': false,
    'type': 'function'
  },
  {
    'inputs': [
      {
        'name': 'candidateNames',
        'type': 'bytes32[]'
      }
    ],
    'payable': false,
    'type': 'constructor'
  }
];

const VotingContract = web3.eth.contract(abi);

// In your nodejs console, execute contractInstance.address to get the address at which
// the contract is deployed and change the line below to use your deployed address
const contractInstance = VotingContract.at('0x2a9c1d265d06d47e8f7b00ffa987c9185aecf672');
const candidates = {
  'Rama': 'candidate-1',
  'Nick': 'candidate-2',
  'Jose': 'candidate-3'
};

function voteForCandidate() {
  candidateName = document.getElementById('candidate').value;
  contractInstance.voteForCandidate(candidateName, {
    from: web3.eth.accounts[0]
  }, function() {
    let div_id = candidates[candidateName];
    document.getElementById('#' + div_id).innerHTML = contractInstance.totalVotesFor.call(candidateName).toString();
  });
}


const candidateNames = Object.keys(candidates);

for (var i = 0; i < candidateNames.length; i++) {
  const name = candidateNames[i];
  const val = contractInstance.totalVotesFor
  .call(name)
  .toString();
  document.getElementById('#' + candidates[name]).innerHTML = val;
};